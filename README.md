Step:

- Pastikan sudah menginstall docker dan docker-compose
- Jalan docker compose file menggunakan command $ docker-compose --build -d
- Cek status menggunakan command $ docker-compose ps
- Silahkan cek akses http://localhost:9090/
